<%@include file="/include/pre_header.jsp" %>

<body>
    <link rel="shortcut icon" type="image/ico" href="favicon_1.ico"/>
    <div class="warper container-fluid">


        <div class="container">

            <div class="row">
                <div class="col-lg-4 col-lg-offset-4">  
                    <div class="page-header">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <h1><strong>&nbsp;&nbsp;&nbsp;SPECS SLA generator</strong> </div>

                    <span class="h5"><br> <img src="../assets/images/avtar/p.png" width="110"> Add credentials and Register now </span>
                    <p class="text-center"></p>
                    <hr class="clean">
                    <!--  standard tag necessario per mantenere l accesso a login_action non � stato utilizzato invece  per raffigurare gli elementi html
                    poiche struts non prevede l uso di placeholder per esempio. -->


                    <!--  da fare jsp x la registrazione quindi da sostituire login_action -->
                    <html:form action="/Login_Action">
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" name="nome" class="form-control"  placeholder="Username" >
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                            <input type="password" name="pass" class="form-control"  placeholder="Password">
                        </div>

                        <button type="submit" class="btn btn-info btn-block" value="Login">Register</button>
                    </html:form>
                    <hr>
                </div>
            </div>
        </div>


    </div>


    <%@include file="/include/post_footer.jsp" %>
    <!-- sezione per aggiungere js specifici per pagina-->

    <!-- fine sezione -->
</body>
</html> 