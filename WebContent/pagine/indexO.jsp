<%@include file="/include/pre_header.jsp" %>
<%@page import="form.Login_Form"%>
<body>	

    <!-- Preloader -->
    <div class="loading-container">
        <div class="loading">
            <div class="l1">
                <div></div>
            </div>
            <div class="l2">
                <div></div>
            </div>
            <div class="l3">
                <div></div>
            </div>
            <div class="l4">
                <div></div>
            </div>
        </div>
    </div>
    <!-- Preloader -->

    <aside class="left-panel">

        <div class="user text-center">
              <h3 class="text-white margin-ridotto"><b>SPECS SLA editor</b></h3>
        </div>

        <nav class="navigation">
            <ul class="list-unstyled">
                <li><a href="<%=request.getContextPath()%>/CreateSDT_Action.do"><i class="fa fa-bookmark-o"></i><span class="nav-label">Create SLA template</span></a></li>
                <li class="has-submenu"><a href="#"><i class="fa fa-arrow-circle-down"></i> <span class="nav-label">Browse & View</span></a>
                    <ul class="list-unstyled">
                        <li><a href="<%=request.getContextPath()%>/BrowseCapabilityOwner_Action.do">Capabilites</a></li>   
                        <li><a href="<%=request.getContextPath()%>/BrowseSLA_Action.do">SLA template</a></li> 
                    </ul>
                </li>
            </ul> 
                
        </nav>
    </aside>
    <%@include file="/include/header.jsp" %>

    <link rel="shortcut icon" type="image/ico" href="favicon_1.ico"/>
    <div class="warper container-fluid">

        <div class="page-header"><h1>Welcome in SPECS SLA generator </h1></div>

        <div class="row"> 
            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-heading">SPECS SLA Generation</div>
                    <div class="panel-body">
                        This application allows to create all resources needed to build Security SLA Templates.
                    </div>

                </div>        

            </div>
        </div>


    </div>

    <!-- Warper Ends Here (working area) -->

    <%@include file="/include/footer.jsp" %>
    <%@include file="/include/post_footer.jsp" %>


    <!-- sezione per aggiungere js specifici per pagina-->

    <!-- fine sezione -->
</body>
</html> 