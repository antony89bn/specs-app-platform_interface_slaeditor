<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html >
<html lang="en">

    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/ico" href="favicon_1.ico"/>
        <title>SPECS SLA editor </title>

        <meta name="description" content="">
        <meta name="author" content="Akshay Kumar">

        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="assets/css/bootstrap/bootstrap.css" /> 

        <!-- Fonts  -->
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300' rel='stylesheet' type='text/css'>

        <!-- Base Styling  -->
        <link rel="stylesheet" href="assets/css/app/app.v1.css" />
        <link href="assets/css/switch-buttons/switch-buttons.css" rel="stylesheet"> 
    </head>
    <body>	


        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-lg-offset-4">   <br>
                    <span class="h4"><br> <img src="img/specs-logo-64.png" width="50"> &nbsp;&nbsp;&nbsp; &nbsp;Welcome in <b>SPECS SLA Editor</b></span>
                    <p class="text-center"></p>
                    <hr class="clean">
<!--  standard tag necessario per mantenere l accesso a login_action non è stato utilizzato invece  per raffigurare gli elementi html
poiche struts non prevede l uso di placeholder per esempio. -->

                    <html:form action="/Login_Action">
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            <input type="text" name="nome" class="form-control"  placeholder="Username">
                        </div>
                        <div class="form-group input-group">
                            <span class="input-group-addon"><i class="fa fa-key"></i></span>
                            <input type="password" name="pass" class="form-control"  placeholder="Password">
                        </div>
                        <div class="form-group input-group">
                            <label for="btnDeveloper"></label>  Developer 
                            <input type="radio" name="selectedItems" value="developer" id="btnDeveloper" checked="">
                            <label for="btnOwner"></label> &nbsp;  Owner
                            <input type="radio" name="selectedItems" value="owner" id="btnOwner">
                        </div>
                        <button type="submit" class="btn btn-primary btn-block" value="Login">Login</button>
                    </html:form>
                

                   <!--     <hr><p class="text-center text-gray">No Account Yet? Register here!</p>              
                        <a href="pagine/register.jsp" class="btn btn-default btn-block">Register</a>  -->
                </div>
            </div>
        </div>


<%@include file="/include/post_footer.jsp" %>
<!-- sezione per aggiungere js specifici per pagina-->

<!-- fine sezione -->
</body>
</html> 