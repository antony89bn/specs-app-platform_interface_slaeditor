<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>SPECS SLA editor</title>

        <link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-top: 60px;
                padding-bottom: 40px;
            }

            .sidebar-nav {
                padding: 9px 0;
            }
        </style>

    </head>
    <!-- ********************************-->

    <body>

        <jsp:include page="../common/navigatorDeveloper.jsp">
            <jsp:param  name="_home" value="class='active'"/>
        </jsp:include>

        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="panel">
            <div class="container">
                <h1>Thank you for creating a new capability!</h1>
             <!--<a class="btn btn-primary btn-large" href="<%=request.getContextPath()%>/CreateSC_Finito_Action.do" target="-self">Invia capability allo SLA-Manager&raquo;</a> -->
                <div>
                    <h4>Insert an ID, a name and a description for the new capability</h4>
                    <html:form action="/CreateSC_Finito_Action">
                        <br/>
                        Id Capability: <html:text property="idCapability" />
                        <br/> <br/>
                        Name Capability <html:text property="nameCapability" />
                        <br/> <br/>
                        Description Capability <html:text property="descriptionCapability" />
                        <br/> <br/>
                        <html:submit title="Invia" />
                    </html:form>
                </div>
            </div>
        </div>

        <jsp:include page="../common/footer.jsp"/>

    </body>
</html>