<%@include file="/include/pre_header.jsp" %>

<%@page import="form.Login_Form"%>
<body>	

    <!-- Preloader -->
    <div class="loading-container">
        <div class="loading">
            <div class="l1">
                <div></div>
            </div>
            <div class="l2">
                <div></div>
            </div>
            <div class="l3">
                <div></div>
            </div>
            <div class="l4">
                <div></div>
            </div>
        </div>
    </div>
    <!-- Preloader -->

    <aside class="left-panel">

        <div class="user text-center">
            <h3 class="text-white margin-ridotto"><b>SPECS SLA editor</b></h3>
        </div>


        <nav class="navigation">
            <ul class="list-unstyled">
                <li><a href="<%=request.getContextPath()%>/CreateSC_Action.do"><i class="fa fa-bookmark-o"></i><span class="nav-label">Create Capability</span></a></li>
                <li><a href="<%=request.getContextPath()%>/AssociateSM_Action.do"><i class="fa fa-bookmark-o"></i><span class="nav-label"> Associate Security Metric</span></a></li>
                <li class="has-submenu"><a href="#"><i class="fa fa-arrow-circle-down"></i> <span class="nav-label">Browse & View</span></a>
                    <ul class="list-unstyled">
                        <li><a href="<%=request.getContextPath()%>/BrowseCapability_Action.do">Capabilites</a></li>
                        <li><a href="<%=request.getContextPath()%>/BrowseSecurityMetric_Action.do">Security Metrics</a></li>
                        <li><a href="<%=request.getContextPath()%>/BrowseMapping_Action.do">Mapping Metrics</a></li>
                    </ul>
                </li>
            </ul> 

        </nav>
    </aside>
    <%@include file="/include/header.jsp" %>

    <link rel="shortcut icon" type="image/ico" href="favicon_1.ico"/>
    <div class="warper container-fluid">

        <div class="page-header"><h1><strong>Associate one Security Metric with a Security Capability</strong></h1></div>

        <div class="row"> 
            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="panel">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5>  In this section, you can associate a security metric to a capability, and in particular to the security controls the Capability is composed of.</h5>

                                    Please follow the next steps:
                                    <br> <br>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <h5><b>1</b>  - Select a Security Metric</h5>
                                            <html:form action="/AssociateSM_Action" >
                                                <div class="form-group">
                                                    <logic:iterate name="smForm" indexId="ite" id="qvalue">
                                                        <p>
                                                        <div class="switch-button info sm showcase-switch-button">
                                                            <html:radio styleClass="form-control" styleId="check${ite}" property="selectedItems" idName="qvalue" value="value"/>
                                                            <label for="check${ite}"></label>
                                                        </div>

                                                        <bean:write name="qvalue" property="value" />
                                                        </p>
                                                    </logic:iterate>
                                                </div>                                                                         
                                        <br>
                                        <html:submit styleClass="btn btn-info" title="Invia" />
                                    </div>
                                    </html:form>
                                  </div>
                          
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h5><b>2</b>  - Select a Security Capability</h5>
                                    <html:form action="/AssociateSM_Action" >
                                        <div class="form-group">                                           
                                            <logic:iterate indexId="ite" name="capabilityForm" id="qvalue">
                                                <p>
                                                <div class="switch-button info sm showcase-switch-button">
                                                    <html:radio styleId="check_${ite}" styleClass="form-control" property="selectedItems2" idName="qvalue" value="value"/>
                                                    <label for="check_${ite}"></label>
                                                </div>
                                                <bean:write name="qvalue" property="value" />
                                                </p>
                                            </logic:iterate>
                                                
                                        </div>  
                                        </br> 
                                            <html:submit styleClass="btn btn-info" title="Invia" />
                                                                    
                                </div> 
                               </html:form> 
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div> 
            </div>
            </div>
        </div>
 

<!-- Warper Ends Here (working area) -->

<%@include file="/include/footer.jsp" %>
<%@include file="/include/post_footer.jsp" %>

<!-- sezione per aggiungere js specifici per pagina-->
<!-- fine sezione -->
</body>
</html> 