<%@include file="/include/pre_header.jsp" %>

<%@page import="form.Login_Form"%>
<body>	

    <!-- Preloader -->
    <div class="loading-container">
        <div class="loading">
            <div class="l1">
                <div></div>
            </div>
            <div class="l2">
                <div></div>
            </div>
            <div class="l3">
                <div></div>
            </div>
            <div class="l4">
                <div></div>
            </div>
        </div>
    </div>
    <!-- Preloader -->

    <aside class="left-panel">

        <div class="user text-center">
            <h3 class="text-white margin-ridotto"><b>SPECS SLA editor</b></h3>
        </div>


        <nav class="navigation">
            <ul class="list-unstyled">
                <li><a href="<%=request.getContextPath()%>/HomeDeveloper_Action.do"><i class="fa fa-bookmark-o"></i><span class="nav-label">Home</span></a></li>
                <li><a href="<%=request.getContextPath()%>/BrowseSecurityMetric_Action.do"><i class="fa fa-bookmark-o"></i><span class="nav-label">View Security Metric</span></a></li>           
            </ul>
        </nav>
    </aside>
    <%@include file="/include/header.jsp" %>

    <link rel="shortcut icon" type="image/ico" href="favicon_1.ico"/>
    <div class="warper container-fluid">

        <div class="page-header"><h1><strong>Navigate security metrics</strong></h1></div>

        <div class="row"> 
            <div class="col-md-12">

                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="panel">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h5>In this section you can visualize stored security metrics and access their details</h5>

                                    Select a security metric
                                    <br> <br>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                       
                                            <html:form action="/BrowseSecurityMetric_Action" >
                                                <div class="form-group">
                                                
                                                    <logic:iterate name="smForm" indexId="ite" id="qvalue">
                                                        <p>
                                                        <div class="switch-button info sm showcase-switch-button">
                                                            
                                                            <html:radio styleClass="form-control" styleId="check${ite}" property="selectedItems" idName="qvalue" value="value"/>
                                                        
                                                        
                                                            <label for="check${ite}"></label>
                                                        </div>

                                                        <bean:write name="qvalue" property="value" />
                                                </p>
                                                    </logic:iterate>
                                                </div>     
                                                <br>
                                        <html:submit styleClass="btn btn-info" title="Invia" />
                                    </html:form>
                                            </div>
                                        </div>
                             
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

<!-- Warper Ends Here (working area) -->

<%@include file="/include/footer.jsp" %>
<%@include file="/include/post_footer.jsp" %>

<!-- sezione per aggiungere js specifici per pagina-->
<!-- fine sezione -->
</body>
</html> 