<%@include file="/include/pre_header.jsp" %>
<aside class="left-panel">
    <div class="user text-center">
        <h3 class="text-white margin-ridotto"><b>SPECS SLA editor</b></h3>
    </div>


    <nav class="navigation">
        <ul class="list-unstyled">
            <li><a href="<%=request.getContextPath()%>/CreateSC_Action.do"><i class="fa fa-bookmark-o"></i><span class="nav-label">Create Capability</span></a></li>
            <li><a href="<%=request.getContextPath()%>/AssociateSM_Action.do"><i class="fa fa-bookmark-o"></i><span class="nav-label"> Associate Security Metric</span></a></li>
            <li class="has-submenu"><a href="#"><i class="fa fa-arrow-circle-down"></i> <span class="nav-label">Browse & View</span></a>
                <ul class="list-unstyled">
                    <li><a href="<%=request.getContextPath()%>/BrowseCapability_Action.do">Capabilites</a></li>
                    <li><a href="<%=request.getContextPath()%>/BrowseSecurityMetric_Action.do">Security Metrics</a></li>
                    <li><a href="<%=request.getContextPath()%>/BrowseMapping_Action.do">Mapping Metrics</a></li>
                </ul>
            </li>
        </ul> 

    </nav>
</aside>
<%@include file="/include/header.jsp" %>

<link rel="shortcut icon" type="image/ico" href="favicon_1.ico"/>
<div class="warper container-fluid">

    <div class="page-header"><h1><strong>Thank you for creating a new capability!</strong></h1></div>

    <div class="row"> 
        <div class="col-md-12">

            <div class="panel panel-default">
                <div class="panel-heading">Please select security controls you are interested in</div>
                <div class="panel-body">

                    <html:form action="/CreateSC_Action">
                        <logic:iterate name="controlli" id="controlli">
                            <p>
                            <label class="cr-styled">
                                <html:multibox property="selectedItems2">
                                    <bean:write name="controlli" property="value" />

                                </html:multibox>
                                <i class="fa"></i>  
                            </label> 

                            <bean:write name="controlli" property="label" />
                            </p>

                        </logic:iterate>
                        <br>
                        <html:submit styleClass="btn btn-info" title="Invia" />
                    </html:form>



                </div>
            </div>

        </div>
    </div>
</div>

<!-- Warper Ends Here (working area) -->

<%@include file="/include/footer.jsp" %>
<%@include file="/include/post_footer.jsp" %>

<!-- sezione per aggiungere js specifici per pagina-->

<!-- fine sezione -->
</body>
</html> 