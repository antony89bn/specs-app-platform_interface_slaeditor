<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TESI APP	</title>

<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>

</head>
<!-- ********************************-->

<body>
<div id="_navigator">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand" src="img/specs-logo-32.png"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="<%=request.getContextPath()%>/HomeOwner_Action.do">Home</a></li>
					<li><a href="<%=request.getContextPath()%>/CreateSDT_Action.do">Create SDT</a></li>
					<li><a href="<%=request.getContextPath()%>/CreateWsagTemplate_Action.do">Crea Wsag template</a></li>
					<li><a href="<%=request.getContextPath()%>/StoreWsag_Action.do">Store Wsag</a></li>
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
</div>

<div class="panel">
      <div class="container">
        <h1>Crea Security Capability</h1>
        <p>In questa sessione posso creare uno SLA per poi salvarlo il un database.</p>
</div>
</div>



<!--     Main jumbotron for a primary marketing message or call to action -->
<!--     <div class="jumbotron"> -->
<!--       <div class="container"> -->
<!--         <h1>In questa pagina puoi creare un Wsag completo e salvarlo</h1> -->
<!--         <div class="panel" id="submitDiv"> -->
<!-- 		   <button class="btn btn-primary" name="metodo" value="singleEvaluation" type="submit">Crea risorsa</button>  -->
<!-- 		</div>	 -->
<!--         <p> -->
<!--          <a class="btn btn-primary btn-large" target="_self">Crea Risorsa&raquo;</a> -->
<!--          </p> -->
<!--          <p> -->
<%--          <a class="btn btn-primary btn-large" href="<%=request.getContextPath()%>/CreateSC_Action.do" target="_self">Crea Security Capability&raquo;</a> --%>
<!--  		</p><p> -->
<!--          <a class="btn btn-primary btn-large" target="_self">Seleziona Security Metric&raquo;</a> -->
<!--          </p>  -->
         
         
         
<!--       </div> -->
<!--     </div> -->

<jsp:include page="../common/footer.jsp"/>

</body>
</html>