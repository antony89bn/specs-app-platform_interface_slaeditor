<%@include file="/include/pre_header.jsp" %>

<%@page import="form.Login_Form"%>
<body>	

    <!-- Preloader -->
    <div class="loading-container">
        <div class="loading">
            <div class="l1">
                <div></div>
            </div>
            <div class="l2">
                <div></div>
            </div>
            <div class="l3">
                <div></div>
            </div>
            <div class="l4">
                <div></div>
            </div>
        </div>
    </div>
    <!-- Preloader -->

    <aside class="left-panel">

          <div class="user text-center">
              <h3 class="text-white margin-ridotto"><b>SPECS SLA editor</b></h3>
        </div>


        <nav class="navigation">
            <ul class="list-unstyled">
                <li><a href="<%=request.getContextPath()%>/HomeOwner_Action.do"><i class="fa fa-bookmark-o"></i><span class="nav-label">Home</span></a></li>
                <li><a href="<%=request.getContextPath()%>/CreateSDT_Action.do"><i class="fa fa-bookmark-o"></i><span class="nav-label">Create SLA template</span></a></li>
                <li><a href="<%=request.getContextPath()%>/StoreWsag_Action.do"><i class="fa fa-bookmark-o"></i><span class="nav-label">Store SLA template</span></a></li>            
            </ul> 

        </nav>
    </aside>
    <%@include file="/include/header.jsp" %>

    <link rel="shortcut icon" type="image/ico" href="favicon_1.ico"/>
    <div class="warper container-fluid">

        <div class="page-header"><h1>Store SLA template </h1></div>

        <div class="row"> 
            <div class="col-md-12">

                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="panel">


                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <p>In this section you can store the built SLA Template in the XML format in a database. Please specify ID, name and a brief description</p> 
                                    <html:form action="/StoreWsag_Action">
                                        Id SLA : <html:text property="idSla" />
                                        <br />
                                        <br />
                                        Name SLA : <html:text property="nameSla" />
                                        <br /><br />
                                        Description SLA : <html:text property="descriptionSla" />
                                        <br />  <br />
                                        <html:submit styleClass="btn btn-info" title="Invia" />
                                    </html:form>


                                </div>
                            </div>

                        </div>
                    </div>

                </div>        

            </div>
        </div>


    </div>

    <!-- Warper Ends Here (working area) -->

    <%@include file="/include/footer.jsp" %>
    <%@include file="/include/post_footer.jsp" %>


    <!-- sezione per aggiungere js specifici per pagina-->

    <!-- fine sezione -->
</body>
</html> 