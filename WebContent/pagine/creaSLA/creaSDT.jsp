<%@include file="/include/pre_header.jsp" %>

<%@page import="form.Login_Form"%>
<body>	

    <!-- Preloader -->
    <div class="loading-container">
        <div class="loading">
            <div class="l1">
                <div></div>
            </div>
            <div class="l2">
                <div></div>
            </div>
            <div class="l3">
                <div></div>
            </div>
            <div class="l4">
                <div></div>
            </div>
        </div>
    </div>
    <!-- Preloader -->

    <aside class="left-panel">
        <div class="user text-center">
            <h3 class="text-white margin-ridotto"><b>SPECS SLA editor</b></h3>
        </div>


        <nav class="navigation">
            <ul class="list-unstyled">
                <li><a href="<%=request.getContextPath()%>/HomeOwner_Action.do"><i class="fa fa-bookmark-o"></i><span class="nav-label">Home</span></a></li>
                <li><a href="<%=request.getContextPath()%>/CreateSDT_Action.do"><i class="fa fa-bookmark-o"></i><span class="nav-label">Create SLA template</span></a></li>
                <li><a href="<%=request.getContextPath()%>/StoreWsag_Action.do"><i class="fa fa-bookmark-o"></i><span class="nav-label">Store SLA template</span></a></li>            
            </ul> 

        </nav>
    </aside>
    <%@include file="/include/header.jsp" %>

    <link rel="shortcut icon" type="image/ico" href="favicon_1.ico"/>
    <div class="warper container-fluid">

        <div class="page-header"><h1>Create Service Description Term</h1></div>

        <div class="row"> 
            <div class="col-md-12">

                <div class="panel panel-default">

                    <div class="panel-body">
                        <div class="panel">


                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h1></h1>
                                    <h5>In this section you can create a new SLA Template. Please insert a new Service Description Term (SDT) section to the template, by specifying the available Cloud resources and the capability to be offered to End-users according to the following steps.</h5>
                                    <br>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <h5><b>1</b>  - Add a Cloud resource section to the SDT</h5>
                                            <p>Note: this application considers a default resource section.</p>
                                            <div class="row">
                                                <div class="col-md-3 ">  
                                                    <a class="btn btn-info btn-block" href="<%=request.getContextPath()%>/CreateResource_Action.do" target="_self">Add resource section</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">

                                        <div class="panel-body">

                                            <h5><b>2</b>  - Add capabilities section to the SDT </h5>
                                            <p>Select capabilities to offer to End-users</p>

                                            <html:form action="/CreateSDT_Action">
                                                <logic:iterate name="capabilityForm" id="capabilityForm">    
                                                    <div class="row">

                                                        <div class="col-md-2">
                                                            <label>
                                                            <bean:write name="capabilityForm" property="label" />
                                                            </label>
                                                        </div>    
                                                        <div class="col-md-1 ">
                                                            <label class="cr-styled">
                                                                <html:multibox styleClass="form-control" property="selectedItems">
                                                                    <bean:write name="capabilityForm" property="value" />
                                                                </html:multibox>

                                                                <i class="fa"></i>  
                                                            </label> 
                                                        </div>
                                                    </div>
                                                </logic:iterate>
                                                <br>
                                                <br>

                                                <html:submit  styleClass="btn btn-info" title="Invia" />

                                            </html:form>
                                        </div>
                                    </div>
                                    <div class="panel panel-default">
                                        <div class="panel-body">

                                            <h5> <b>3</b> - Add a security metric section to the SDT</h5>
                                            <p>Note that all security metrics associated with the selected capabilities will be included</p>
                                            <div class="row">
                                                <div class="col-md-3 ">  
                                                    <a class="btn btn-info btn-block" href="<%=request.getContextPath()%>/SelectSecurityMetric_Action.do" target="_self">Add security metrics</a>
                                                </div>
                                            </div></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>        

            </div>
        </div>


    </div>

    <!-- Warper Ends Here (working area) -->

    <%@include file="/include/footer.jsp" %>
    <%@include file="/include/post_footer.jsp" %>


    <!-- sezione per aggiungere js specifici per pagina-->

    <!-- fine sezione -->
</body>
</html> 