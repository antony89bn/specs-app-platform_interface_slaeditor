<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TESI APP	</title>

<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>

</head>
<!-- ********************************-->

<body>

<jsp:include page="../common/nologin_navigator.jsp">
<jsp:param  name="_home" value="class='active'"/>
</jsp:include>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h1>Crea constraints</h1>
        <p>In questa sessione puoi creare un nuovo Service Description Term, un Guarantee Terms ed i Constraints</p>
        
        <html:form action="/CreateConstraints_Action" >
        
        <!--  
        <html:multibox property="selectedItems" value="A" /> Maria
		<html:multibox property="selectedItems" value="K" /> Klaus
		<html:multibox property="selectedItems" value="P" /> Peter
        <html:submit property="btnApply"/>
	    -->
	    
	    
	    <logic:iterate name="customers" id="customers">
	    <html:multibox property="selectedItems">
			<bean:write name="customers" property="value" /> 
		</html:multibox>
			<bean:write name="customers" property="label" />
	    </logic:iterate>
	    <br />
	    <html:submit property="btnApply"/>
	    
	    
		</html:form>
        
      </div>
    </div>

<jsp:include page="../common/footer.jsp"/>

</body>
</html>