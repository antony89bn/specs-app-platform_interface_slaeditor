<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>SPECS SLA editor	</title>

<link href="bootstrap/css/bootstrap.min.css"  type="text/css" rel="stylesheet">
<style type="text/css">
	body {
		padding-top: 60px;
		padding-bottom: 40px;
	}
	
	.sidebar-nav {
		padding: 9px 0;
	}
</style>

</head>
<!-- ********************************-->

<body>
<jsp:include page="/pagine/common/navigatorDeveloper.jsp">
<jsp:param  name="_home" value="class='active'"/>
</jsp:include>
<div class="panel">
      <div class="container">
        <h1>Welcome to the SPECS SLA-Editor application for Developers</h1>
        <h3>
        <p>This application allows developers to create new <b>security capabilities</b>, defined as sets of <b>security controls</b> specified in well-known standard frameworks (e.g., CSA's CCM and NIST Framework).
        </br>Moreover, the application enables to associate existing capabilities with proper <b>security metrics</b>, previously specified in a standard format through an external application.
         </h3>
        </br>
         <h4>
        Available functionalities can be found on the menu tabs. 
 		</h4>
</div>
</div>


<jsp:include page="../common/footer.jsp"/>

</body>
</html>