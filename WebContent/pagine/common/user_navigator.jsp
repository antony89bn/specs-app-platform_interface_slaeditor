<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<div id="_navigator">
	<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand" src="<%=request.getContextPath()%>/img/specs-logo-32.png"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li ${param._home}><a href="<%=request.getContextPath()%>">Home</a></li>
					<li ${param._rank}><a href="<%=request.getContextPath()%>/user/ListFileEvaluationAction.do">Evaluate</a></li>
				</ul>
				<a class="navbar-brand pull-right" href="#">Guest User</a>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>
</div>

