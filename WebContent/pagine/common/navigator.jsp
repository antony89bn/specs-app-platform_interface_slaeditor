<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>

<div id="_navigator">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed"
                        data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                        aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span> <span
                        class="icon-bar"></span> <span class="icon-bar"></span> <span
                        class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"> <img alt="Brand" src="<%=request.getContextPath()%>/img/specs-logo-32.png"></a>
            </div>
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li ${param._home}><a href="<%=request.getContextPath()%>">Home</a></li>
                    <li ${param._rank}><a href="<%=request.getContextPath()%>/user/ListFileEvaluationAction.do">Evaluate</a></li>  
                    <!--<li ${param._rank}><a href="<%=request.getContextPath()%>/pages/admin/uploadCaiq/uploadCaiq.jsp">Upload CAIQ</a></li>  -->
                    <li ${param._neg}><a href="<%=request.getContextPath()%>/pages/admin/uploadCaiq/uploadCaiqFromXml.jsp">Upload CAIQ </a></li>
                    <li ${param._sign}><a href="<%=request.getContextPath()%>/admin/ListEditCaiqAction.do">Edit CAIQ</a></li>
                    <li ${param._impl}><a href="<%=request.getContextPath()%>/pages/admin/uploadJudgment/uploadJudgment.jsp">Upload Importances</a></li>
                    <li ${param._obs}><a href="<%=request.getContextPath()%>/admin/ListEditJudgmentAction.do">Edit Imortances</a></li>
                </ul>
                <a class="navbar-brand pull-right" href="#">Expert User</a>

            </div>
            <!--/.nav-collapse -->
        </div>
    </nav>
</div>

