

<%@include file="/include/pre_header.jsp" %>
<%@include file="/include/navbar.jsp" %>
<%@include file="/include/header.jsp" %>

<link rel="shortcut icon" type="image/ico" href="favicon_1.ico"/>
<div class="warper container-fluid">

    <div class="page-header"><h1>Welcome in SPECS SLA editor </h1></div>

    <div class="row"> 
        <div class="col-md-12">
            
                	<div class="panel panel-default">
                      <div class="panel-heading">SPECS SLA editor</div>
                      <div class="panel-body">
                       This application allows to create all resources needed to build Security SLA Templates.
                      </div>
                    </div>        

    </div>
    </div>


</div>

<!-- Warper Ends Here (working area) -->

<%@include file="/include/footer.jsp" %>
<%@include file="/include/post_footer.jsp" %>


<!-- sezione per aggiungere js specifici per pagina-->

<!-- fine sezione -->
</body>
</html> 