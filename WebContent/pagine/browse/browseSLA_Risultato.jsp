<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TESI APP</title>

<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

</head>
<!-- ********************************-->

<body ng-app="SPECS-APP-SLA-EDITOR">

	<jsp:include page="../common/nologin_navigator.jsp">
		<jsp:param name="_home" value="class='active'" />
	</jsp:include>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="panel" ng-controller="slaEditContr">
		<div class="container">
			<div>
				<h1>Browse SLA Risultato</h1>

				<pre>
        <c:out value="prova" />
        </pre>
			</div>
			
			<div class="panel panel-default" ng-show="metricTemplate">
				<div class="panel-heading">
					<h2>Retrieved Metric</h2>
				</div>
				<div class="panel-body ">
					<div class="well" ng-show="metricUserName">
						<b>Metric Name:</b>
					</div>
				</div>
			</div>
			
			
			
			
			
			
			
			
			
			
			
			
			
		</div>
	</div>

	<jsp:include page="../common/footer.jsp" />


	<script  src="<%=request.getContextPath()%>/js/angular.js"></script>
	<!-- *************************** -->
	<script src="<%=request.getContextPath()%>/js/jquery.xml2json.js"></script>
	<script src="<%=request.getContextPath()%>/js/jquery.min.js"></script>
	<script src="<%=request.getContextPath()%>/js/xml2json.js"></script>
	<script src="<%=request.getContextPath()%>/js/xml2json2.js"></script>
	<script src="<%=request.getContextPath()%>/js/jquery.xpath.js"></script>
	
	<script src="<%=request.getContextPath()%>/js/bootstrap_wizard/jquery.bootstrap.wizard.js"></script>
	
	<!-- Page constructors -->
	<!-- ANGULAR SCRIPT -->
	<script>
		var app = angular.module('SPECS-APP-SLA-EDITOR', []);

		app.controller('slaEditContr', [ '$scope', function($scope) {
			console.log("chiamata inizializzazione angular");
			$scope.xmlRetrieved = unescape(${pippo});
			console.log($scope.xmlRetrieved);
		   
			$scope.metricTemplate = $.parseXML($scope.xmlRetrieved);
		    
		    $scope.abstractMetric = $.xml2json($scope.metricTemplate);
			
		    console.log($scope.abstractMetric);
		} ]);
	</script>



</body>
</html>