<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean"%>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html"%>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>TESI APP</title>

<link href="bootstrap/css/bootstrap.min.css" type="text/css"
	rel="stylesheet">
<style type="text/css">
body {
	padding-top: 60px;
	padding-bottom: 40px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

</head>
<!-- ********************************-->

<body>
	<div id="_navigator">
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"> <img alt="Brand"
					src="img/specs-logo-32.png"></a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="./">Home</a></li>
					<li><a
						href="<%=request.getContextPath()%>/BrowseSLA_Action.do">View
							SLA</a></li>
					<li><a
						href="<%=request.getContextPath()%>/DeleteSLA_Action.do">Delete
							SLA</a></li>

				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
		</nav>
	</div>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="panel">
		<div class="container">
			<h1>Delete SLA</h1>
			<p>In questa sessione puoi eliminare gli SLA completi
				precedentemente creati</p>

			<html:form action="/DeleteSLA_Action">
				<logic:iterate name="wsagForm" id="qvalue">
					<html:radio
							styleClass="form-control" property="selectedItems"
							idName="qvalue" value="value" /> <bean:write name="qvalue"
							property="value" /> <br />
					
				</logic:iterate>
				<html:submit title="Invia" />
			</html:form>
		</div>
	</div>

	<jsp:include page="../common/footer.jsp" />

</body>
</html>