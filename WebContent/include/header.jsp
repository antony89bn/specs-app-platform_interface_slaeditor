<!--  Header della pagina qui comparira' il logout per l' utente loggato  -->
<section class="content">
    <header class="top-head container-fluid">
        <button type="button" class="navbar-toggle pull-left">
        </button>
        <nav class=" navbar-default hidden-xs" role="navigation">
        </nav>

        <ul class="nav-toolbar">
            <li class="dropdown">
                <a href="<%=request.getContextPath()%>/pagine/logout.jsp">
                    <span class="h6"> <i class="fa fa-user-times fa-2x" aria-hidden="true"></i>
                    </span> </a>
            </li>
        </ul>
    </header>
    <!-- Header Ends -->




