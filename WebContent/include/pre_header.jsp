<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<!--  Pre_header: contiene l head della pagina HTML  -->
<html lang="en">
    <!--  le diverse pagine HTML sono strutturate in modo flessibile, infatti, dividendo il codice HTML in pagine, 
    si può inportare solo dove serve il codice inerente una pagina es. non serve la navbar nella pagina login -->
    <head>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" type="image/ico" href="favicon_1.ico"/>
        <title>SLA editor </title>
        <meta name="description" content="sla_editor">
        <meta name="author" content="Antonietta Fabris">

        <link rel="shortcut icon" type="image/ico" href="favicon_1.ico"/>
        
        <!-- Bootstrap core CSS -->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/bootstrap/bootstrap.css" /> 
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,500,600,700,300' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/app/app.v1.css" />
        <link href="<%=request.getContextPath()%>/assets/css/switch-buttons/switch-buttons.css" rel="stylesheet">
    </head>