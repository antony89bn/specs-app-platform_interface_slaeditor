    
<!-- JQuery v1.9.1 -->
<script src="<%=request.getContextPath()%>/assets/js/jquery/jquery-1.9.1.min.js" type="text/javascript"></script>
<script src="<%=request.getContextPath()%>/assets/js/plugins/underscore/underscore-min.js"></script>
<!-- Bootstrap -->
<script src="<%=request.getContextPath()%>/assets/js/bootstrap/bootstrap.min.js"></script>

<!-- Globalize -->
<script src="<%=request.getContextPath()%>/assets/js/globalize/globalize.min.js"></script>

<!-- NanoScroll -->
<script src="<%=request.getContextPath()%>/assets/js/plugins/nicescroll/jquery.nicescroll.min.js"></script>


<!-- Custom JQuery -->
<script src="<%=request.getContextPath()%>/assets/js/app/custom.js" type="text/javascript"></script>


