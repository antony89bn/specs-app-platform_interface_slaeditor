package form;

import org.apache.struts.action.ActionForm;

public class AssociateSM_Form extends ActionForm {
	
private String selectedItems =null;
private String selectedItems2 =null;
private String[] selectedItems3={};
private String nome=null;
	
	public String getSelectedItems(){
		return selectedItems;
	}
	
	public void setSelectedItems(String selectedItems){
		this.selectedItems=selectedItems;
	}
	
	public String getSelectedItems2(){
		return selectedItems2;
	}
	
	public void setSelectedItems2(String selectedItems2){
		this.selectedItems2=selectedItems2;
	}
	
	public String[] getSelectedItems3(){
		return selectedItems3;
	}
	
	public void setSelectedItems3(String[] selectedItems3){
		this.selectedItems3=selectedItems3;
	}
	
	public String getNome(){
		return nome;
	}
	
	public void setNome(String nome){
		this.nome=nome;
	}
	
}