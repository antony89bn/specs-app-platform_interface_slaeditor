package form;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class CreateSDT_Form extends ActionForm {
	
private String [] selectedItems ={};
	
	
	public String[] getSelectedItems(){
		return selectedItems;
	}
	
	public void setSelectedItems(String []selectedItems){
		this.selectedItems=selectedItems;
	}
}