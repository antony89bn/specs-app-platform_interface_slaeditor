package form;

import org.apache.struts.action.ActionForm;

public class CreateWsagTemplate_Form extends ActionForm{
	
	private String[] selectedItems1 ={};
	
	
	public String[] getSelectedItems1(){
		return selectedItems1;
	}
	
	public void setSelectedItems1(String[] selectedItems){
		this.selectedItems1=selectedItems;
	}
	
	private String[] selectedItems2={};
	
	public String[] getSelectedItems2(){
		return selectedItems2;
	}
	
	public void setSelectedItems2(String[] selectedItems){
		this.selectedItems2=selectedItems;
	}
	
	

}
