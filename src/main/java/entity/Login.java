package entity;

import db.DBLogin;

public class Login {
	
	private String username;
	private String password;
	private String ruolo;
	private int login;
	
	public Login(){
		
	}
	
	public Login(String username, String password, String ruolo){
		this.username=username;
		this.password=password;
		this.ruolo=ruolo;
		DBLogin dbLogin=new DBLogin(username,password,ruolo);
		login=dbLogin.verificaLogin();
	}
	
	public void setLogin(int login){
		this.login=login;
	}
	
	public void setUSername(String username){
		this.username=username;
	}
	
	public void setPassword(String password){
		this.password=password;
	}
	
	public void setRuolo(String ruolo){
		this.ruolo=ruolo;
	}
	
	public int getLogin(){
		return this.login;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	public String getPassword(){
		return this.password;
	}
	
	public String getRuolo(){
		return this.ruolo;
	}

}
