package entity;

import java.util.ArrayList;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import db.DBCapability;
import db.DBConnectionManager;
import db.DBControllo;
import db.DBMappingCapMetric;

public class MappingCapMetric {
	
	private String capability;
	private String id;
	private String metrica;
	private String[] controlli;
	private String controllo;
	private String mappingDescription;
	private Document document;
	private Element root;
	private Element idElement;
	private Element capabilityElement;
	private Element controlloElement;
	private Element metricaElement;
	
	
	
	public MappingCapMetric(){	
	}
	
	public MappingCapMetric(String nome, String capability, String[] controlli, String metrica){
		this.id=nome;
		this.capability=capability;
		this.controlli=controlli;
		this.metrica=metrica;
//		System.err.println("Sono nella classe Entity MappingCapMetric");
		
		controllo=controlli[0];
		for(int i=1;i<controlli.length;i++){
			controllo=controllo+" "+controlli[i]+" ";
		}
		
		
		
		this.mappingDescription=creaDescrizioneMapping(nome,capability,controllo,metrica);		
		System.out.println(mappingDescription);
		DBMappingCapMetric dbMCM=new DBMappingCapMetric(nome,capability,controllo,metrica,mappingDescription);
		
	}
	
	
	
	public ArrayList<MappingCapMetric> catturaAllMapping(){
		int count=0;
		int campi=5;
		DBMappingCapMetric dbMapping=new DBMappingCapMetric();
		ArrayList<MappingCapMetric> arrayListMapping= new ArrayList<MappingCapMetric>();
		ArrayList arrayList= new ArrayList();
		arrayList=dbMapping.catturaAllMapping();
		for(int i=0;i<arrayList.size();i=i+campi){
			MappingCapMetric mappCapMetric=new MappingCapMetric();
			arrayListMapping.add(mappCapMetric);
			arrayListMapping.get(count).capability=(String)arrayList.get(i);
			arrayListMapping.get(count).controllo=(String)arrayList.get(i+1);
			arrayListMapping.get(count).metrica=(String)arrayList.get(i+2);
			arrayListMapping.get(count).id=(String)arrayList.get(i+3);
			arrayListMapping.get(count).mappingDescription=(String)arrayList.get(i+4);
			count++;
		}
		return arrayListMapping;
	}
	
	
	
	
	
	public String creaDescrizioneMapping(String nome, String Capability, String controllo, String Metrica){
		String mappingDescription=null;
		 
		root=new Element("root");
		document=new Document(root);
		idElement=new Element("idMapping");
		capabilityElement=new Element("capability");
		controlloElement =new Element("controllo"); 
		metricaElement =new Element("metrica");
		
		idElement.setText(nome);
		capabilityElement.setText(Capability);
		controlloElement.setText(controllo);
		metricaElement.setText(Metrica);
		root.addContent(idElement);
		root.addContent(capabilityElement);
		root.addContent(controlloElement);
		root.addContent(metricaElement);
		
		XMLOutputter outputter = new XMLOutputter();
		outputter.setFormat(Format.getPrettyFormat());
		mappingDescription=outputter.outputString(document);
		
		
		return mappingDescription;
	}
	
	public ArrayList<MappingCapMetric> catturaSecurityMetric(String[] capability){
		int count =0;
		int campi =3;
		DBMappingCapMetric dbMappingCapMetric=new DBMappingCapMetric();
		ArrayList<MappingCapMetric> arrayListMapping = new ArrayList<MappingCapMetric>(); //arrayList che mi ritorna che in ogni locazione ho un controllo
		ArrayList arrayList=new ArrayList();								//arrayList generico che mi ritorna dalla funzione dbControllo.caturaControlli
		
		arrayList=dbMappingCapMetric.catturaSecurityMetric(capability);
		for(int i=0;i<arrayList.size();i=i+campi){
			MappingCapMetric mappingCapMetric = new MappingCapMetric();
			arrayListMapping.add(mappingCapMetric);
			arrayListMapping.get(count).capability=(String)arrayList.get(i);
			arrayListMapping.get(count).controllo=(String)arrayList.get(i+1);
			arrayListMapping.get(count).metrica=(String)arrayList.get(i+2);
			count++;
		}
		return arrayListMapping;
	}
	
	public ArrayList<MappingCapMetric> catturaSecurityMetricSingolaCapability(String capability){
		int count =0;
		int campi =3;
		DBMappingCapMetric dbMappingCapMetric=new DBMappingCapMetric();
		ArrayList<MappingCapMetric> arrayListMapping = new ArrayList<MappingCapMetric>(); //arrayList che mi ritorna che in ogni locazione ho un controllo
		ArrayList arrayList=new ArrayList();								//arrayList generico che mi ritorna dalla funzione dbControllo.caturaControlli
		
		arrayList=dbMappingCapMetric.catturaSecurityMetricSingolaCapability(capability);
		for(int i=0;i<arrayList.size();i=i+campi){
			MappingCapMetric mappingCapMetric = new MappingCapMetric();
			arrayListMapping.add(mappingCapMetric);
			arrayListMapping.get(count).capability=(String)arrayList.get(i);
			arrayListMapping.get(count).controllo=(String)arrayList.get(i+1);
			arrayListMapping.get(count).metrica=(String)arrayList.get(i+2);
			count++;
		}
		return arrayListMapping;
	}
	
	
	
	
	public String getIdMapping(){
		return this.id;
	}
	
	public String getMappingDescription(){
		return this.mappingDescription;
	}
	
	public String getCapability(){
		return this.capability;
	}
	
	public String getControllo(){
		return this.controllo;
	}
	
	public String getMetrica(){
		return this.metrica;
	}
	
}
