package logic;


public class AssociateMetricControl {
	
	private static AssociateMetricControl istanza;
	private String securityMetric;
	private String capability;
	private String id;
	private String[] controlli;
	
	//costruttore di default
		public AssociateMetricControl(){	
		}
		//ritorna l'istanza
		public static AssociateMetricControl getInstance(){
			if(istanza==null){
				istanza=new AssociateMetricControl();
			}
			return istanza;
		}
		
		public void setSecurityMetric(String securityMetric){
			this.securityMetric=securityMetric;
		}
		
		public String getSecurityMetric(){
			return securityMetric;
		}
		
		public void setCapability(String capability){
			this.capability=capability;
		}
		
		public String getCapability(){
			return capability;
		}
		
		public void setId(String id){
			this.id=id;
		}
		
		public String getId(){
			return this.id;
		}
		
		public void setControlli(String[] controlli){
			this.controlli=controlli;
		}
		
		public String[] getControlli(){
			return this.controlli;
		}

}
