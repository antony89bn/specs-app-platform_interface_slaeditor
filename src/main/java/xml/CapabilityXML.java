package xml;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import entity.Capability;
import entity.Controllo;

public class CapabilityXML {
	
	
	
	public CapabilityXML(){
	}
	
	
	public ArrayList<Controllo> estraiControlli(String controlliXML) throws ParserConfigurationException, SAXException, IOException{
		ArrayList<Controllo> arrayListControllo=new ArrayList<>();
		String idControl=null;
		String controlName=null;
		String familyId=null;
		String familyName=null;
		int control=0;
		int enhancement=0;
		String description=null;
		
		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(controlliXML));
		Document doc=db.parse(is);
		Node root=doc.getFirstChild();
		int count =0;
		for(int i=0;i<root.getChildNodes().getLength();i++){
			Node securityControl=root.getChildNodes().item(i);
			description=securityControl.getTextContent();
			if(securityControl.hasAttributes()){

				idControl=securityControl.getAttributes().item(0).getNodeValue();
				controlName=securityControl.getAttributes().item(1).getNodeValue();
				familyId=securityControl.getAttributes().item(3).getNodeValue();
				familyName=securityControl.getAttributes().item(4).getNodeValue();
				try {
					control = Integer.parseInt(securityControl.getAttributes().item(5).getNodeValue());
					enhancement = Integer.parseInt(securityControl.getAttributes().item(2).getNodeValue());
				} catch (Exception e) { 
					e.printStackTrace(); 
				}
				Controllo controllo=new Controllo();
				arrayListControllo.add(controllo);
				arrayListControllo.get(count).setIdControl(idControl);
				arrayListControllo.get(count).setControlName(controlName);
				arrayListControllo.get(count).setFamilyId(familyId);
				arrayListControllo.get(count).setFamilyName(familyName);
				arrayListControllo.get(count).setControl(control);
				arrayListControllo.get(count).setEnhancement(enhancement);
				arrayListControllo.get(count).setDescription(description);
				count++;
				System.out.println("Stampo id "+idControl);
				System.out.println("Stampo name "+controlName);
				System.out.println("Stampo familyId "+familyId);
				System.out.println("Stampo familyName "+familyName);
				System.out.println("Stampo control "+control);
				System.out.println("Stampo enhancement "+enhancement);
				System.out.println("Stampo la descrizione "+description);
			}
		}
		for (int i=0; i<arrayListControllo.size();i++){
			System.out.println("Stampo id "+arrayListControllo.get(i).getIdControl());
			System.out.println("Stampo name "+arrayListControllo.get(i).getControlName());
			System.out.println("Stampo familyId "+arrayListControllo.get(i).getFamilyId());
			System.out.println("Stampo familyName "+arrayListControllo.get(i).getFamilyName());
			System.out.println("Stampo control "+arrayListControllo.get(i).getControl());
			System.out.println("Stampo enhancement "+arrayListControllo.get(i).getEnhancement());
			System.out.println("Stampo la descrizione "+arrayListControllo.get(i).getDescription());
		}
		return arrayListControllo;
	}
}
