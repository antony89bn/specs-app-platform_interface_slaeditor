package xml;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.struts.action.Action;
import org.jdom2.*;
import org.jdom2.output.*;
import org.jdom2.JDOMException;

import entity.Controllo;

import java.io.*;
import java.util.ArrayList;

import entity.*;

public class JDOMWriter {
	
	private boolean configurazioneBase=false;
	private static JDOMWriter istanza;
	//private ArrayList<Controllo> arrayListControllo;
	private String[] capabilitySelezionate;
	private String filePath;
	private String nomeWsag;
	private Document document;
	private Element root;
	private Element name;
	private Element context;
	private Element terms;
	private Element all;
	private Element sdt;
	private Element serviceReference;
	private Element serviceDescription;
	private Element capabilities;
	private Element security_metrics;
	private Element service_resources;
	private Namespace nsWsag,nsSpecs,nsNist,nsXs,nsXsi;
	
	
	//costruttore di default
	public JDOMWriter(){	
	}
	//ritorna l'istanza
	public static JDOMWriter getInstance(){
		if(istanza==null){
			istanza=new JDOMWriter();
		}
		return istanza;
	}
	
	//costruttore
	public void salvaNomeControlli(String [] capabilitySelezionate){
		this.capabilitySelezionate=capabilitySelezionate;
	}
	
	public String[] getCapabilities(){
		return this.capabilitySelezionate;
	}

	public void salvaSecurityMetric(){
	}
	
	public void salvaNomeWsag(String nome){
		this.nomeWsag=nome;
		name.addContent(nomeWsag);
	}

	public void configBase() throws IOException{		
		//if(configurazioneBase==false){
		//namespace
		nsWsag=Namespace.getNamespace("wsag", "http://schemas.ggf.org/graap/2007/03/ws-agreement");
		nsSpecs=Namespace.getNamespace("specs", "http://specs-project.eu/schemas/sla_related");
		nsNist=Namespace.getNamespace("nist", "http://specs-project.eu/schemas/nist_control_framework");	
		nsXs=Namespace.getNamespace("xs", "http://www.w3.org/2001/XMLSchema");
		nsXsi=Namespace.getNamespace("xsi","http://www.w3.org/2001/XMLSchema-instance"); 
		
		//elemento radice
		root = new Element("AgreementOffer",nsWsag);
		root.addNamespaceDeclaration(nsNist);	//agg i Namespace
		root.addNamespaceDeclaration(nsSpecs);
		root.addNamespaceDeclaration(nsXs);
		root.addNamespaceDeclaration(nsXsi);
		
		//documento
		document = new Document(root);
		
		name=new Element ("Name",nsWsag);
		//name.addContent(nomeWsag);
		
		context=new Element("Context",nsWsag);
		terms=new Element("Terms",nsWsag);
		
		root.addContent(name);
		root.addContent(context);
		root.addContent(terms);
		//campo CONTEXT
		Element context1=new Element("AgreementInitiator",nsWsag);
		context1.addContent("$SPECS-CUSTOMER");
		Element context2=new Element("AgreementResponder",nsWsag);
		context2.addContent("$SPECS-APPLICATION");
		Element context3=new Element("ServiceProvider",nsWsag);
		context3.addContent("AgreementResponder");
		Element context4=new Element("ExpirationTime",nsWsag);
		context4.addContent("2014-02-02T06:00:00");
		Element context5=new Element("TemplateName",nsWsag);
		context5.addContent("Y2-APP-TEMPLATE");
		context.addContent(context1);
		context.addContent(context2);
		context.addContent(context3);
		context.addContent(context4);
		context.addContent(context5);

		
		all= new Element("All",nsWsag);
		terms.addContent(all);
		sdt=new Element("ServiceDescriptionTerm",nsWsag);
		sdt.setAttribute("Name", "Secure Web Server",nsWsag);
		sdt.setAttribute("ServiceName", "SecureWebServer",nsWsag);
		all.addContent(sdt);
		
		serviceDescription= new Element("serviceDescription",nsSpecs);
		sdt.addContent(serviceDescription);
		capabilities =new Element("capabilities",nsSpecs);
		service_resources=new Element("service_resources",nsSpecs);
		serviceDescription.addContent(service_resources);
		serviceDescription.addContent(capabilities);
		security_metrics=new Element("security_metrics",nsSpecs);
		serviceDescription.addContent(security_metrics);
	}
	
	public void creaRisorsa(){
//		service_resources=new Element("service_resources",nsSpecs);
//		serviceDescription.addContent(service_resources);
		Element res1= new Element("resources_provider",nsSpecs);
		res1.setAttribute("id", "aws-ec2");
		res1.setAttribute("name", "Amazon");
		res1.setAttribute("zone", "us-east-1");
		service_resources.addContent(res1);
		Element vm= new Element("VM",nsSpecs);
		vm.setAttribute("appliance", "us-east-1/ami-ff0e0696");
		vm.setAttribute("hardware", "t1.micro");
		vm.setAttribute("descr", "open suse 13.1 on amazon EC2");
		res1.addContent(vm);
		
		
	}


	public void salvaXMLcapability(ArrayList<Controllo> arrayListControllo, String nome){
			
			Element capability=new Element("capability",nsSpecs);
			capability.setAttribute("id", nome,nsSpecs);
			capability.setAttribute("name", nome,nsSpecs);
			capability.setAttribute("description", "this capability .......",nsSpecs);
			capabilities.addContent(capability);
			for(int i=0; i<arrayListControllo.size();i++){
			Element item1= new Element("security_control",nsSpecs);
			item1.setAttribute("id", arrayListControllo.get(i).getIdControl());
			item1.setAttribute("name",arrayListControllo.get(i).getControlName());
			item1.setAttribute("control_family_id",arrayListControllo.get(i).getFamilyId(),nsNist);
			item1.setAttribute("control_family_name",arrayListControllo.get(i).getFamilyName(),nsNist);
			String stringa1=Integer.toString(arrayListControllo.get(i).getControl());
			String stringa2=Integer.toString(arrayListControllo.get(i).getEnhancement());
			item1.setAttribute("security_control", stringa1,nsNist);
			item1.setAttribute("control_enhancement", stringa2,nsNist);
			Element descr1 = new Element("control_description",nsSpecs);
			descr1.setText(arrayListControllo.get(i).getDescription());
			item1.addContent(descr1);
			capability.addContent(item1);
			}    
	}
	
	public void salvaSecurityMetric(ArrayList<SecurityMetric> arrayListSM){
		Element security_metric = new Element("security_metric",nsSpecs);
		security_metric.setAttribute("name",arrayListSM.get(0).getName());
		security_metric.setAttribute("referenceId",arrayListSM.get(0).getReferenceId());
		security_metrics.addContent(security_metric);
		Element metricDefinition = new Element("metricDefinition",nsSpecs);
		security_metric.addContent(metricDefinition);
		Element unit = new Element("unit",nsSpecs);
		unit.setAttribute("name", arrayListSM.get(0).getUnitName());
		metricDefinition.addContent(unit);
		Element intervalUnit = new Element("intervalUnit",nsSpecs);
		unit.addContent(intervalUnit);
		Element intervalItemsType = new Element("intervalItemsType");
		intervalItemsType.setText(arrayListSM.get(0).getIntervalType());
		Element intervalItemStart = new Element("intervalItemStart");
		intervalItemStart.setText(arrayListSM.get(0).getIntervalStart());
		Element intervalItemStop = new Element("intervalItemStop");
		intervalItemStop.setText(arrayListSM.get(0).getIntervalStop());
		Element intervalItemStep = new Element("intervalItemStep");
		intervalItemStep.setText(arrayListSM.get(0).getIntervalSpep());
		intervalUnit.addContent(intervalItemsType);
		intervalUnit.addContent(intervalItemStart);
		intervalUnit.addContent(intervalItemStop);
		intervalUnit.addContent(intervalItemStep);
		Element scale = new Element("scale",nsSpecs);
		metricDefinition.addContent(scale);
		Element quantitative = new Element("Quantitative",nsSpecs);
		quantitative.setText(arrayListSM.get(0).getScale());
		scale.addContent(quantitative);
		Element expression = new Element("expression",nsSpecs);
		expression.setText(arrayListSM.get(0).getEspression());
		Element definition = new Element("definition",nsSpecs);
		definition.setText(arrayListSM.get(0).getDefinition());
		Element note = new Element("note",nsSpecs);
		metricDefinition.addContent(expression);
		metricDefinition.addContent(definition);
		metricDefinition.addContent(note);
		
		Element metricRules = new Element("metricRules",nsSpecs);
		security_metric.addContent(metricRules);
		Element metricRule = new Element("metricRule",nsSpecs);
		metricRule.setText(arrayListSM.get(0).getRuleId());
		metricRules.addContent(metricRule);
		Element ruleDescription = new Element("ruleDescription",nsSpecs);
		ruleDescription.setText(arrayListSM.get(0).getRuleDescription());
		Element ruleValue = new Element("metricRules",nsSpecs);
		ruleValue.setText(arrayListSM.get(0).getRuleValue());
		metricRule.addContent(ruleDescription);
		metricRule.addContent(ruleValue);
		
		Element metricParameters = new Element("metricParameters",nsSpecs);
		metricParameters.setAttribute("paramId",arrayListSM.get(0).getParamId());
		Element paramDescription = new Element("paramDescription",nsSpecs);
		Element paramType = new Element("paramType",nsSpecs);
		Element paramValue = new Element("paramValue",nsSpecs);
		paramDescription.setText(arrayListSM.get(0).getParamDescription());
		paramType.setText(arrayListSM.get(0).getParamType());
		paramValue.setText(arrayListSM.get(0).getParamValue());
		metricParameters.addContent(paramDescription);
		metricParameters.addContent(paramType);
		metricParameters.addContent(paramValue);
		security_metric.addContent(metricParameters);
	}
	
	public void creaServiceReference(){
		serviceReference= new Element("ServiceReference",nsWsag);
		serviceReference.setAttribute("Name", "SecureWebServer_endpoint", nsWsag);
		serviceReference.setAttribute("ServiceName", "SecureWebServer", nsWsag);
		Element specsEndpoint=new Element("endpoint",nsSpecs);
		specsEndpoint.setText("http://specs/application-endpoint");
		serviceReference.addContent(specsEndpoint);
		all.addContent(serviceReference);
		
	}
	
	public void creaServiceProperties(String capability, ArrayList<MappingCapMetric> arrayListMapping){
		Element serviceProprieties=new Element("ServicesProprieties",nsWsag);
		Element variableSet= new Element("VariableSet",nsWsag);
		serviceProprieties.addContent(variableSet);
		
		serviceProprieties.setAttribute("Name", "//specs:capability"+capability,nsWsag);
		serviceProprieties.setAttribute("ServiceName", "Secure"+capability,nsWsag);
		for(int i=0;i<arrayListMapping.size();i++){
			Element variable=new Element("Variable",nsWsag);
			variable.setAttribute("Name", "specs_"+arrayListMapping.get(i).getCapability(), nsWsag);
			variable.setAttribute("Metric", arrayListMapping.get(i).getMetrica(), nsWsag);
			Element location=new Element("Location",nsWsag);
			location.setText("SPECS:Security Control = "+arrayListMapping.get(i).getControllo());
			variable.addContent(location);
			variableSet.addContent(variable);
		}
		
		all.addContent(serviceProprieties);

	}
	
	public void creaGT(String capability,ArrayList<SLO> arrayListSlo){
		Element guaranteeTerm=new Element("GuaranteeTerm",nsWsag);
		guaranteeTerm.setAttribute("Name", "//specs:capability"+capability, nsWsag);
		guaranteeTerm.setAttribute("Obligated", "ServiceProvider", nsWsag);
		all.addContent(guaranteeTerm);
		
		Element serviceLevelObjective=new Element("ServiceLevelObjective",nsWsag);
		Element customServiceLevel=new Element("CustomServiceLevel",nsWsag);
		Element objectiveList=new Element("objectiveList",nsWsag);
		
		for(int i=0;i<arrayListSlo.size();i++){
			Element slo=new Element("SLO",nsSpecs);
			slo.setAttribute("SLO_ID",arrayListSlo.get(i).getIdSLO());
			objectiveList.addContent(slo);
			Element metricREF=new Element("MetricREF",nsSpecs);
			metricREF.setText(arrayListSlo.get(i).getIdMetric());
			slo.addContent(metricREF);
			Element sloExpression=new Element("SLOexpression",nsSpecs);
			slo.addContent(sloExpression);
			Element oneOpExpression=new Element("oneOpExpression",nsSpecs);
			sloExpression.addContent(oneOpExpression);
			oneOpExpression.setAttribute("operator",arrayListSlo.get(i).getOperator());
			oneOpExpression.setAttribute("operand",arrayListSlo.get(i).getOperand());
			Element importance=new Element("importance_weight",nsSpecs);
			importance.setText(arrayListSlo.get(i).getImportance());
			slo.addContent(importance);
		}
		
		customServiceLevel.addContent(objectiveList);
		serviceLevelObjective.addContent(customServiceLevel);
		guaranteeTerm.addContent(serviceLevelObjective);
		
		Element businessValueList=new Element("BusinessValueList",nsWsag);
		guaranteeTerm.addContent(businessValueList);
		
		
	}

	public void creaServiceProperties_WebPool(){
		Element serviceProprieties=new Element("ServicesProprieties",nsWsag);
		Element variableSet= new Element("VariableSet",nsWsag);
		serviceProprieties.addContent(variableSet);
		
		serviceProprieties.setAttribute("Name", "//specs:capability[@id='WEBPOOL']",nsWsag);
		serviceProprieties.setAttribute("ServiceName", "SecureWebServer",nsWsag);
		Element variable1=new Element("Variable",nsWsag);
		variable1.setAttribute("Name", "specs_webpool_M1", nsWsag);
		variable1.setAttribute("Metric", "specs.eu/metrics/M1_redudancy", nsWsag);
		Element location1=new Element("Location",nsWsag);
		location1.setText("//specs:security_control[@id='WEBPOOL_NIST_CP_2'] | //specs:security_control[@id='WEBPOOL_NIST_SC_5'] | //specs:security_control[@id='WEBPOOL_NIST_SC_36']");
		variable1.addContent(location1);
		variableSet.addContent(variable1);
		
		Element variable2=new Element("Variable",nsWsag);
		variable2.setAttribute("Name", "specs_webpool_M2", nsWsag);
		variable2.setAttribute("Metric", "specs.eu/metrics/M2_diversity", nsWsag);
		Element location2=new Element("Location",nsWsag);
		location2.setText("//specs:security_control[@id='WEBPOOL_NIST_CP_2'] | //specs:security_control[@id='WEBPOOL_NIST_SC_5'] | //specs:security_control[@id='WEBPOOL_NIST_SC_29']");
		variable2.addContent(location2);
		variableSet.addContent(variable2);
		all.addContent(serviceProprieties);

	}

	public void creaServiceProperties_Sva(){
		Element serviceProprieties=new Element("ServicesProprieties",nsWsag);
		Element variableSet= new Element("VariableSet",nsWsag);
		serviceProprieties.addContent(variableSet);
		
		serviceProprieties.setAttribute("Name", "//specs:capability[@id='SVA']",nsWsag);
		serviceProprieties.setAttribute("ServiceName", "SecureWebServer",nsWsag);
		Element variable1=new Element("Variable",nsWsag);
		variable1.setAttribute("Name", "specs_sva_M13", nsWsag);
		variable1.setAttribute("Metric", "specs.eu/metrics/M13_vulnerability_report_max_age", nsWsag);
		Element location1=new Element("Location",nsWsag);
		location1.setText("//specs:security_control[@id='WEBPOOL_NIST_CP_2'] | //specs:security_control[@id='WEBPOOL_NIST_SC_5'] | //specs:security_control[@id='WEBPOOL_NIST_SC_36']");
		variable1.addContent(location1);
		variableSet.addContent(variable1);
		
		Element variable2=new Element("Variable",nsWsag);
		variable2.setAttribute("Name", "specs_sva_M14", nsWsag);
		variable2.setAttribute("Metric", "specs.eu/metrics/M14_vulnerability_list_max_age", nsWsag);
		Element location2=new Element("Location",nsWsag);
		location2.setText("//specs:security_control[@id='OPENVAS_NIST_CA_7_3'] | //specs:security_control[@id='OPENVAS_NIST_RA_5_1']");
		variable2.addContent(location2);
		variableSet.addContent(variable2);
		all.addContent(serviceProprieties);
	}

	public void creaServiceProperties_OpenVas(){
		Element serviceProprieties=new Element("ServicesProprieties",nsWsag);
		Element variableSet= new Element("VariableSet",nsWsag);
		serviceProprieties.addContent(variableSet);
		
		serviceProprieties.setAttribute("Name", "//specs:capability[@id='OPENVAS']",nsWsag);
		serviceProprieties.setAttribute("ServiceName", "SecureWebServer",nsWsag);
		Element variable1=new Element("Variable",nsWsag);
		variable1.setAttribute("Name", "specs_openvas_M13", nsWsag);
		variable1.setAttribute("Metric", "specs.eu/metrics/M13_vulnerability_report_max_age", nsWsag);
		Element location1=new Element("Location",nsWsag);
		location1.setText("//specs:security_control[@id='OPENVAS_NIST_CA_7'] | //specs:security_control[@id='OPENVAS_NIST_CA_8'] | //specs:security_control[@id='OPENVAS_NIST_RA_5']");
		variable1.addContent(location1);
		variableSet.addContent(variable1);
		
		Element variable2=new Element("Variable",nsWsag);
		variable2.setAttribute("Name", "specs_openvas_M14", nsWsag);
		variable2.setAttribute("Metric", "specs.eu/metrics/M14_vulnerability_list_max_age", nsWsag);
		Element location2=new Element("Location",nsWsag);
		location2.setText("//specs:security_control[@id='OPENVAS_NIST_CA_7_3'] | //specs:security_control[@id='OPENVAS_NIST_RA_5_1']");
		variable2.addContent(location2);
		variableSet.addContent(variable2);
		all.addContent(serviceProprieties);

	}

	public void creaServiceProperties_Ossec(){
		Element serviceProprieties=new Element("ServicesProprieties",nsWsag);
		Element variableSet= new Element("VariableSet",nsWsag);
		serviceProprieties.addContent(variableSet);
		
		serviceProprieties.setAttribute("Name", "//specs:capability[@id='OSSEC']",nsWsag);
		serviceProprieties.setAttribute("ServiceName", "SecureWebServer",nsWsag);
		Element variable1=new Element("Variable",nsWsag);
		variable1.setAttribute("Name", "specs_ossec_M15", nsWsag);
		variable1.setAttribute("Metric", "specs.eu/metrics/M15_dos_report_max_age", nsWsag);
		Element location1=new Element("Location",nsWsag);
		location1.setText("//specs:security_control[@id='WEBPOOL_NIST_CP_2'] | //specs:security_control[@id='WEBPOOL_NIST_SC_5'] | //specs:security_control[@id='WEBPOOL_NIST_SC_36']");
		variable1.addContent(location1);
		variableSet.addContent(variable1);
		all.addContent(serviceProprieties);

	}
	
	public void creaGT_WebPool(){
		Element guaranteeTerm=new Element("GuaranteeTerm",nsWsag);
		guaranteeTerm.setAttribute("Name", "//specs:capability[@id='WEBPOOL']", nsWsag);
		guaranteeTerm.setAttribute("Obligated", "ServiceProvider", nsWsag);
		all.addContent(guaranteeTerm);
		
		Element serviceScope=new Element("ServiceScope",nsWsag);
		serviceScope.setAttribute("ServiceName", "SecureWebServer", nsWsag);
		Element qualifyingCondition=new Element("QualifyingCondition",nsWsag);
		qualifyingCondition.setText("TRUE");
		guaranteeTerm.addContent(serviceScope);
		guaranteeTerm.addContent(qualifyingCondition);
		
		Element serviceLevelObjective=new Element("ServiceLevelObjective",nsWsag);
		Element customServiceLevel=new Element("CustomServiceLevel",nsWsag);
		Element objectiveList=new Element("objectiveList",nsWsag);
		Element slo1=new Element("SLO",nsSpecs);
		slo1.setAttribute("name","specs_webpool_M1");
		Element expression1=new Element("expression",nsSpecs);
		expression1.setAttribute("unit", "machines");
		expression1.setAttribute("op","eq");
		expression1.setText("3");
		Element importanceWeight1=new Element("importance_weight",nsSpecs);
		importanceWeight1.setText("MEDIUM");
		slo1.addContent(expression1);
		slo1.addContent(importanceWeight1);
		objectiveList.addContent(slo1);
		
		Element slo2=new Element("SLO",nsSpecs);
		slo2.setAttribute("name","specs_webpool_M2");
		Element expression2=new Element("expression",nsSpecs);
		expression2.setAttribute("unit", "machines");
		expression2.setAttribute("op","eq");
		expression2.setText("2");
		Element importanceWeight2=new Element("importance_weight",nsSpecs);
		importanceWeight2.setText("MEDIUM");
		slo2.addContent(expression2);
		slo2.addContent(importanceWeight2);
		objectiveList.addContent(slo2);
		
		customServiceLevel.addContent(objectiveList);
		serviceLevelObjective.addContent(customServiceLevel);
		guaranteeTerm.addContent(serviceLevelObjective);
		
		Element businessValueList=new Element("BusinessValueList",nsWsag);
		guaranteeTerm.addContent(businessValueList);
		
		
	}
	
	public void creaGT_Sva(){
		Element guaranteeTerm=new Element("GuaranteeTerm",nsWsag);
		guaranteeTerm.setAttribute("Name", "//specs:capability[@id='SVA']", nsWsag);
		guaranteeTerm.setAttribute("Obligated", "ServiceProvider", nsWsag);
		all.addContent(guaranteeTerm);
		
		Element serviceScope=new Element("ServiceScope",nsWsag);
		serviceScope.setAttribute("ServiceName", "SecureWebServer", nsWsag);
		Element qualifyingCondition=new Element("QualifyingCondition",nsWsag);
		qualifyingCondition.setText("TRUE");
		guaranteeTerm.addContent(serviceScope);
		guaranteeTerm.addContent(qualifyingCondition);
		
		Element serviceLevelObjective=new Element("ServiceLevelObjective",nsWsag);
		Element customServiceLevel=new Element("CustomServiceLevel",nsWsag);
		Element objectiveList=new Element("objectiveList",nsWsag);
		Element slo1=new Element("SLO",nsSpecs);
		slo1.setAttribute("name","specs_sva_M13");
		Element expression1=new Element("expression",nsSpecs);
		expression1.setAttribute("unit", "hours");
		expression1.setAttribute("op","eq");
		expression1.setText("24");
		Element importanceWeight1=new Element("importance_weight",nsSpecs);
		importanceWeight1.setText("MEDIUM");
		slo1.addContent(expression1);
		slo1.addContent(importanceWeight1);
		objectiveList.addContent(slo1);
		
		Element slo2=new Element("SLO",nsSpecs);
		slo2.setAttribute("name","specs_sva_M14");
		Element expression2=new Element("expression",nsSpecs);
		expression2.setAttribute("unit", "hours");
		expression2.setAttribute("op","eq");
		expression2.setText("72");
		Element importanceWeight2=new Element("importance_weight",nsSpecs);
		importanceWeight2.setText("MEDIUM");
		slo2.addContent(expression2);
		slo2.addContent(importanceWeight2);
		objectiveList.addContent(slo2);
		
		customServiceLevel.addContent(objectiveList);
		serviceLevelObjective.addContent(customServiceLevel);
		guaranteeTerm.addContent(serviceLevelObjective);
		
		Element businessValueList=new Element("BusinessValueList",nsWsag);
		guaranteeTerm.addContent(businessValueList);
		
	}
	
	public void creaGT_OpenVas(){
		Element guaranteeTerm=new Element("GuaranteeTerm",nsWsag);
		guaranteeTerm.setAttribute("Name", "//specs:capability[@id='OPENVAS']", nsWsag);
		guaranteeTerm.setAttribute("Obligated", "ServiceProvider", nsWsag);
		all.addContent(guaranteeTerm);
		
		Element serviceScope=new Element("ServiceScope",nsWsag);
		serviceScope.setAttribute("ServiceName", "SecureWebServer", nsWsag);
		Element qualifyingCondition=new Element("QualifyingCondition",nsWsag);
		qualifyingCondition.setText("TRUE");
		guaranteeTerm.addContent(serviceScope);
		guaranteeTerm.addContent(qualifyingCondition);
		
		Element serviceLevelObjective=new Element("ServiceLevelObjective",nsWsag);
		Element customServiceLevel=new Element("CustomServiceLevel",nsWsag);
		Element objectiveList=new Element("objectiveList",nsWsag);
		Element slo1=new Element("SLO",nsSpecs);
		slo1.setAttribute("name","specs_openvas_M13");
		Element expression1=new Element("expression",nsSpecs);
		expression1.setAttribute("unit", "hours");
		expression1.setAttribute("op","eq");
		expression1.setText("24");
		Element importanceWeight1=new Element("importance_weight",nsSpecs);
		importanceWeight1.setText("MEDIUM");
		slo1.addContent(expression1);
		slo1.addContent(importanceWeight1);
		objectiveList.addContent(slo1);
		
		Element slo2=new Element("SLO",nsSpecs);
		slo2.setAttribute("name","specs_openvas_M14");
		Element expression2=new Element("expression",nsSpecs);
		expression2.setAttribute("unit", "hours");
		expression2.setAttribute("op","eq");
		expression2.setText("72");
		Element importanceWeight2=new Element("importance_weight",nsSpecs);
		importanceWeight2.setText("MEDIUM");
		slo2.addContent(expression2);
		slo2.addContent(importanceWeight2);
		objectiveList.addContent(slo2);
		
		customServiceLevel.addContent(objectiveList);
		serviceLevelObjective.addContent(customServiceLevel);
		guaranteeTerm.addContent(serviceLevelObjective);
		
		Element businessValueList=new Element("BusinessValueList",nsWsag);
		guaranteeTerm.addContent(businessValueList);
	}
	
	public void creaGT_Ossec(){
		Element guaranteeTerm=new Element("GuaranteeTerm",nsWsag);
		guaranteeTerm.setAttribute("Name", "//specs:capability[@id='OSSEC']", nsWsag);
		guaranteeTerm.setAttribute("Obligated", "ServiceProvider", nsWsag);
		all.addContent(guaranteeTerm);
		
		Element serviceScope=new Element("ServiceScope",nsWsag);
		serviceScope.setAttribute("ServiceName", "SecureWebServer", nsWsag);
		Element qualifyingCondition=new Element("QualifyingCondition",nsWsag);
		qualifyingCondition.setText("TRUE");
		guaranteeTerm.addContent(serviceScope);
		guaranteeTerm.addContent(qualifyingCondition);
		
		Element serviceLevelObjective=new Element("ServiceLevelObjective",nsWsag);
		Element customServiceLevel=new Element("CustomServiceLevel",nsWsag);
		Element objectiveList=new Element("objectiveList",nsWsag);
		Element slo1=new Element("SLO",nsSpecs);
		slo1.setAttribute("name","specs_ossec_M15");
		Element expression1=new Element("expression",nsSpecs);
		expression1.setAttribute("unit", "hours");
		expression1.setAttribute("op","eq");
		expression1.setText("24");
		Element importanceWeight1=new Element("importance_weight",nsSpecs);
		importanceWeight1.setText("MEDIUM");
		slo1.addContent(expression1);
		slo1.addContent(importanceWeight1);
		objectiveList.addContent(slo1);
	
		
		customServiceLevel.addContent(objectiveList);
		serviceLevelObjective.addContent(customServiceLevel);
		guaranteeTerm.addContent(serviceLevelObjective);
		
		Element businessValueList=new Element("BusinessValueList",nsWsag);
		guaranteeTerm.addContent(businessValueList);
		
		
	}
	
	
	
	
	
	public String convertiXML(){
		String stringXML;
		XMLOutputter outputter = new XMLOutputter();
		outputter.setFormat(Format.getPrettyFormat());
		stringXML=(outputter.outputString(document));
		return stringXML;
	}
	
	public void salvaSuFile() throws FileNotFoundException, IOException{
		XMLOutputter outputter = new XMLOutputter();
		outputter.setFormat(Format.getPrettyFormat());
		outputter.output(document, new FileOutputStream("WsagTemplateAPP.xml"));
	}
	
	public void pulisciTemplate(){
		//arrayListControllo=null;
		istanza=null;
		
		
	}
	
}
	
//public void creaXMLcapability(){
//	
//	nsSpecs=Namespace.getNamespace("specs", "http://specs-project.eu/schemas/sla_related");
//	nsNist=Namespace.getNamespace("nist", "http://specs-project.eu/schemas/nist_control_framework");
//	
//	capability = new Element("capabilities",nsSpecs);
//	capability.addNamespaceDeclaration(nsSpecs);
//	capability.addNamespaceDeclaration(nsNist);
//	document = new Document(capability);
//
//	for(int i=0; i<arrayListControllo.size();i++){
//	Element item1= new Element("security_control",nsSpecs);
//	item1.setAttribute("id", arrayListControllo.get(i).getIdControl());
//	item1.setAttribute("name",arrayListControllo.get(i).getControlName());
//	item1.setAttribute("control_family_id",arrayListControllo.get(i).getFamilyId(),nsNist);
//	item1.setAttribute("control_family_name",arrayListControllo.get(i).getFamilyName(),nsNist);
//	String stringa1=Integer.toString(arrayListControllo.get(i).getControl());
//	String stringa2=Integer.toString(arrayListControllo.get(i).getEnhancement());
//	item1.setAttribute("security_control", stringa1,nsNist);
//	item1.setAttribute("control_enhancement", stringa2,nsNist);
//	Element descr1 = new Element("control_description",nsSpecs);
//	descr1.setText(arrayListControllo.get(i).getDescription());
//	item1.addContent(descr1);
//	capability.addContent(item1);
//	}
//
//	//Creazione dell'oggetto XMLOutputter 
//	XMLOutputter outputter = new XMLOutputter(); 
//    //Imposto il formato dell'outputter come "bel formato" 
//    outputter.setFormat(Format.getPrettyFormat()); 
//    try {
//    	 outputter.output(document, new FileOutputStream(filePath+"cazzo.xml")); 
//		//outputter.output(document, System.out);
//	} catch (IOException e) {
//		// TODO Auto-generated catch block
//		e.printStackTrace();
//	}
//    
//}
	
	
	
	
	
	
	
//		FileOutputStream prova=new FileOutputStream(filePath+"cazzo.xml");
//		PrintStream scrivi = new PrintStream (prova);
//		scrivi.print("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<tns:ValueTree xmlns:tns=\"ValueTree\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"ValueTree ValueTree.xsd \">\n</tns:ValueTree>");
//		scrivi.close();
		
//		//Creazione dell'oggetto XMLOutputter 
//		XMLOutputter outputter = new XMLOutputter(); 
//	    //Imposto il formato dell'outputter come "bel formato" 
//	    outputter.setFormat(Format.getPrettyFormat()); 
//	    //Produco l'output sul file xml.prova
//	    outputter.output(document, new FileOutputStream(filePath+"cazzo.xml")); 
//	    System.out.println("File creato:"); 
//	    //Stampo l'output anche sullo standard output 
//	    outputter.output(document, System.out);
//		}
//		catch (FileNotFoundException e) {
//			File fileTemp = new File(filePath+"cazzo.xml");
//			if (fileTemp.exists()){
//			    fileTemp.delete();
//			}
//			System.out.println("Problema creazione file xml");
//			e.printStackTrace();
//		} catch (IOException e) {
//			File fileTemp = new File(filePath+"cazzo.xml");
//			if (fileTemp.exists()){
//			    fileTemp.delete();
//			}
//			e.printStackTrace();
//		} 
	
