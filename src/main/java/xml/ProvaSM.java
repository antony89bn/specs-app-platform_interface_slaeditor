package xml;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import entity.SecurityMetric;


public class ProvaSM {
	private static String xml;

	public static void main(String[] args) throws JDOMException, IOException{
		SAXBuilder builder = new SAXBuilder(); 
		Document document = builder.build(new File("SLAtemplate_securityMetric2.xml")); 
//		Node root = ((Node) doc).getFirstChild();
//		String xml;
//		
		
		
		XMLOutputter outputter = new XMLOutputter();
		outputter.setFormat(Format.getPrettyFormat());
		xml=(outputter.outputString(document));
		//System.out.println(xml);
		SecurityMetric securityM = new SecurityMetric("Level of Diversity",xml);
		System.err.println(xml);

	}

}
