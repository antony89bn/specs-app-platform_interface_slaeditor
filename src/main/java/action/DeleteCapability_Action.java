package action;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import db.DBCapability;
import entity.Capability;
import form.DeleteCapability_Form;;

public class DeleteCapability_Action extends Action{
	
	public Collection capabilityForm=new ArrayList();
	public ArrayList<Capability> arrayListCapability= new ArrayList<Capability>();
	public ArrayList<Capability> arrayListCapabilityPulite= new ArrayList<Capability>();

	
	
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		System.out.println("Sono nella classe DELETE CAPABILITY ACTION");
		
		String capabilitySelezionate=null;
		
		Capability capability=new Capability();
		arrayListCapability=capability.catturaCapability();
		
		capabilityForm.clear();
		for(int i=0;i<arrayListCapability.size();i++){
			capabilityForm.add(new LabelValueBean(arrayListCapability.get(i).getIdCapability() , arrayListCapability.get(i).getIdCapability()));
		}
		request.setAttribute("capabilityForm", capabilityForm);
		
		
		
		if(form!=null){
			DeleteCapability_Form deleteCapabilityForm=(DeleteCapability_Form)form;
			capabilitySelezionate=deleteCapabilityForm.getSelectedItems();
			
			if(capabilitySelezionate!=null){
				System.out.println(capabilitySelezionate);
			DBCapability dbCapability=new DBCapability();
			dbCapability.eliminaCapability(capabilitySelezionate);
			}
		}
		
		
		return mapping.findForward("success");
	}

}
