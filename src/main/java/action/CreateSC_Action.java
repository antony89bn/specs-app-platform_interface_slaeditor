package action;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import entity.*;
import form.CreateSC_Form;


public class CreateSC_Action  extends Action {
	
	public Collection controlli=new ArrayList();
	public ArrayList<Controllo> arrayListControllo= new ArrayList<Controllo>();
	public ArrayList<Controllo> arrayListControlloPuliti= new ArrayList<Controllo>();
	
	/**
	 * This method performs the override of the execute method of class Action and gives in output an ActionForward.
	 * It allow to login and save user in the session.   
	 *
	 * @param mapping        The ActionMapping.
	 * @param form        The ActionForm.
	 * @param request        The HttpServletRequest.
	 * @param response        The HttpServletResponse.      
	 * @return       The ActionForward
	 * @throws Exception the exception
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
			
		System.out.println("Sono nella classe Create Security Capability");
		String returnMapping="";
		String[] controlliSelezionati = null;
		String[] controlliSelezionati2 = null;
		
		String nome=null;
	
		boolean aggiornamento=false;
		//		//initial the customers collection
		System.out.println("Sono nel form NULL");	
		controlli.clear();
		controlli.add(new LabelValueBean("Access Control", "AC"));
		controlli.add(new LabelValueBean("Awareness and Traing", "AT"));
		controlli.add(new LabelValueBean("Audit and Accountability", "AU"));
		controlli.add(new LabelValueBean("Security Assessment and Authorization", "CA"));
		controlli.add(new LabelValueBean("Configuration Management", "CM"));
		controlli.add(new LabelValueBean("Contigency Planning", "CP"));
		controlli.add(new LabelValueBean("Identification and Authentication", "IA"));
		controlli.add(new LabelValueBean("Incident Response", "IR"));
		controlli.add(new LabelValueBean("Maintenance", "MA"));
		controlli.add(new LabelValueBean("Media Protection", "MP"));
		controlli.add(new LabelValueBean("Physical and Enviromental Protection", "PE"));
		controlli.add(new LabelValueBean("Planning", "PL"));
		controlli.add(new LabelValueBean("Personnel Security", "PS"));
		controlli.add(new LabelValueBean("Risk Assessment", "RA"));
		controlli.add(new LabelValueBean("System and Services Acquisition", "SA"));
		controlli.add(new LabelValueBean("System and Communication Protection", "SC"));
		controlli.add(new LabelValueBean("System and Information Integrity", "SI"));
		//controlli.add(new LabelValueBean("Program Management", "PM"));
		
		//set customer collection in the request
		request.setAttribute("controlli", controlli);
		returnMapping="success";
		
		System.out.println("Ho fatto la request setAttribute");	
		
		if(form!=null){
			
			System.out.println("Sono nel form NON-NULL");
			//effettuo il cast del form in ingresso
			CreateSC_Form createSCform = (CreateSC_Form)form;
			controlliSelezionati=createSCform.getSelectedItems();
			controlliSelezionati2=createSCform.getSelectedItems2();
			
			//stampo la size dell'arreiList dei controlli
//			for(int i=0;i<controlliSelezionati2.length;i++){
//				System.out.println(controlliSelezionati2[i]);
//		    }
			
		    Controllo controllo= new Controllo();
		    
		    if(controlliSelezionati.length!=0){
		    	controllo.catturaControlli(controlliSelezionati);
				arrayListControllo=controllo.catturaControlli(controlliSelezionati);
				controlliSelezionati=null;
		    }
			if(arrayListControllo.size()!=0){
				aggiornamento=true;
				returnMapping="update";
				System.out.println("Aggiornamento");
				controlli.clear();
			}
			if(controlliSelezionati2.length!=0){
				for(int i=0;i<controlliSelezionati2.length;i++){
					System.out.println(controlliSelezionati2[i]);
			    }
//				for(int i=0; i<arrayListControllo.size();i++){
//					System.out.println(arrayListControllo.get(i).getIdControl());
//					System.out.println(arrayListControllo.get(i).getControlName());
//				}
				arrayListControlloPuliti=pulisciControlli(controlliSelezionati2,arrayListControllo);
				
				
				returnMapping="finish"; 
				controlli.clear();
				
				String filePath = getServlet().getServletContext().getRealPath("/")
						+ "XMLFiles/";
				File folder = new File(filePath);
				if (!folder.exists()) {
					folder.mkdir();
				}
				File newFile=new File(filePath,"cazzo.xml");
				ControlliPuliti controlliPuliti=new ControlliPuliti(arrayListControlloPuliti,filePath);
				//aggiunto
				arrayListControllo.clear();
			}
			
			
			
		
		}

		for(int i=0; i<arrayListControllo.size();i++){
			//controlli.add(new LabelValueBean(arrayListControllo.get(i).getIdControl()+" - "+arrayListControllo.get(i).getControlName()+"  : "+arrayListControllo.get(i).getDescription() , arrayListControllo.get(i).getIdControl()));
			controlli.add(new LabelValueBean(arrayListControllo.get(i).getIdControl()+" - "+arrayListControllo.get(i).getControlName() , arrayListControllo.get(i).getIdControl()));
			//			System.out.println(arrayListControllo.get(i).getIdControl());
//			System.out.println(arrayListControllo.get(i).getControlName());
	    	
		}
		System.out.println(getServlet().getServletContext().getRealPath("/"));
		return mapping.findForward(returnMapping);
		

		
		
	}
	
	public ArrayList<Controllo> pulisciControlli(String [] controlliSelezionati2, ArrayList<Controllo> arrayListControllo){
		ArrayList<Controllo> arrayListControlloPuliti = new ArrayList<Controllo>();
		int k=0;
		for(int i=0;i<arrayListControllo.size();i++){
			for(int j=0; j<controlliSelezionati2.length;j++){
				if(controlliSelezionati2[j].compareTo(arrayListControllo.get(i).getIdControl())==0){
					//System.out.println("sono uguali");
					arrayListControlloPuliti.add(arrayListControllo.get(i));
					k++;
				}
			}
		}
		
//		System.out.println("Stampo arrayListPULITO");
//		for(int i=0; i<arrayListControlloPuliti.size();i++){
//			System.out.println(arrayListControlloPuliti.get(i).getIdControl());
//			System.out.println(arrayListControlloPuliti.get(i).getControlName());
//			System.out.println(arrayListControlloPuliti.get(i).getFamilyId());
//			System.out.println(arrayListControlloPuliti.get(i).getFamilyName());
//			System.out.println(arrayListControlloPuliti.get(i).getControl());
//			System.out.println(arrayListControlloPuliti.get(i).getEnhancement());
//			System.out.println(arrayListControlloPuliti.get(i).getDescription());
//		}
		
		
		
		return arrayListControlloPuliti;
	}
	
	
	
	
}