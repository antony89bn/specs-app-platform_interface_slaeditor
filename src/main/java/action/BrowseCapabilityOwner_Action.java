package action;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import entity.Capability;
import entity.Controllo;
import form.BrowseCapability_Form;

public class BrowseCapabilityOwner_Action extends Action {
	
	private String name=null;
	public int oldLength=0;
	public boolean settato=false;
	public Collection capabilityForm=new ArrayList();
	public ArrayList<Capability> arrayListCapability= new ArrayList<Capability>();
	public ArrayList<Capability> arrayListCapabilityPulite= new ArrayList<Capability>();
	
	/**
	 * This method performs the override of the execute method of class Action and gives in output an ActionForward.
	 * It allow to login and save user in the session.   
	 *
	 * @param mapping        The ActionMapping.
	 * @param form        The ActionForm.
	 * @param request        The HttpServletRequest.
	 * @param response        The HttpServletResponse.      
	 * @return       The ActionForward
	 * @throws Exception the exception
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
	System.out.println("Sono nella classe Browse Capability Action");
	String returnMapping="";
	String  capabilitySelezionate=null;
	
	
	Capability capability=new Capability();
	arrayListCapability=capability.catturaCapability();
	
	capabilityForm.clear();
	for(int i=0;i<arrayListCapability.size();i++){
	capabilityForm.add(new LabelValueBean("Capability "+arrayListCapability.get(i).getIdCapability() , arrayListCapability.get(i).getIdCapability()));
	}
	request.setAttribute("capabilityForm", capabilityForm);
	returnMapping="success";
	
	
	if(form!=null){
	BrowseCapability_Form browseCapabilityForm=(BrowseCapability_Form)form;
	capabilitySelezionate=browseCapabilityForm.getSelectedItems();
	System.err.println(capabilitySelezionate);
	
	//this.arrayListCapabilityPulite=pulisciCapability(capabilitySelezionate,arrayListCapability);
	
	String prova=null;
	if(capabilitySelezionate!=null){
	for(int i=0;i<arrayListCapability.size();i++){
		if(capabilitySelezionate.compareTo(arrayListCapability.get(i).getIdCapability())==0)
			prova=arrayListCapability.get(i).getCapabilityDescription();
		}
	}
	if(prova!=null){
//		response.setContentType("text/xml");
//		PrintWriter out= response.getWriter();
//		out.println(prova);
//		out.close();
		//prova.replace("nist:", "nist_");
		String provaReplace;
		
		provaReplace=prova.replace("nist:", "nist");
		System.out.println(prova);
		request.setAttribute("pippo","\""+StringEscapeUtils.escapeJava(provaReplace)+"\"");
		returnMapping="update";
		
		
	}
	
	
//	if(arrayListCapabilityPulite.size()!=0){
//		PrintWriter out= response.getWriter();
//		for(int i=0;i<arrayListCapabilityPulite.size();i++){
//			prova=arrayListCapabilityPulite.get(i).getCapabilityDescription().toString();
//			out.println(prova);
//			//response.getWriter().print(prova);
//		}
//		out.close();
//		returnMapping="update";
//		}
	
	}
		return mapping.findForward(returnMapping);
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setName(String name){
		this.name=name;
	}
	
	public ArrayList<Capability> pulisciCapability(String [] capabilitySelezionate, ArrayList<Capability> arrayListCapability){
		ArrayList<Capability> arrayListCapabilityPulite = new ArrayList<Capability>();
		int k=0;
		for(int i=0;i<arrayListCapability.size();i++){
			for(int j=0; j<capabilitySelezionate.length;j++){
				if(capabilitySelezionate[j].compareTo(arrayListCapability.get(i).getIdCapability())==0){
					//System.out.println("sono uguali");
					arrayListCapabilityPulite.add(arrayListCapability.get(i));
					k++;
				}
			}
		}
	return arrayListCapabilityPulite;
	}
}
