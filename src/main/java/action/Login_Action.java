package action;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import db.DBLogin;
import entity.Login;
import form.Login_Form;



public class Login_Action extends Action{

	/**
	 * This method performs the override of the execute method of class Action and gives in output an ActionForward.
	 * It allow to login and save user in the session.   
	 *
	 * @param mapping        The ActionMapping.
	 * @param form        The ActionForm.
	 * @param request        The HttpServletRequest.
	 * @param response        The HttpServletResponse.      
	 * @return       The ActionForward
	 * @throws Exception the exception
	 */
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
					throws Exception {
		
		System.out.println("Sono nella classe Action LOGIN");
		String returnMapping="error";
		String username=null;
		String pass=null;
		String ruolo=null;
		
		if(form!=null){
			Login_Form loginForm= (Login_Form)form;
			username=loginForm.getNome(); 
			pass=loginForm.getPass();
			ruolo=loginForm.getSelectedItems();
			if(username!=null & pass!=null & ruolo!=null){
				System.out.println(username);
				System.out.println(pass);
				System.out.println(ruolo);
				Login login=new Login(username,pass,ruolo);
				if(login.getLogin()==3){
					if(ruolo.compareTo("user")==0)
						returnMapping="user";
					if(ruolo.compareTo("developer")==0)
						returnMapping="developer";
					if(ruolo.compareTo("owner")==0)
						returnMapping="owner";
				}else{
					returnMapping="error";
				}
			}
		}
		return mapping.findForward(returnMapping);
	}

}

