package action;

import java.util.ArrayList;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.*;
import org.apache.struts.util.LabelValueBean;
import logic.AddCapability;
import entity.Capability;
import form.CreateSDT_Form;


public class CreateSDT_Action  extends Action {
	
	public Collection capabilityForm=new ArrayList();
	public ArrayList<Capability> arrayListCapability= new ArrayList<Capability>();
	
	/**
	 * This method performs the override of the execute method of class Action and gives in output an ActionForward.
	 * It allow to login and save user in the session.   
	 *
	 * @param mapping        The ActionMapping.
	 * @param form        The ActionForm.
	 * @param request        The HttpServletRequest.
	 * @param response        The HttpServletResponse.      
	 * @return       The ActionForward
	 * @throws Exception the exception
	 */
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		System.err.println("Sono in SDT Action");
		String[]  capabilitySelezionate=null;
		
		
		Capability capability=new Capability();
		arrayListCapability=capability.catturaCapability();
		
		capabilityForm.clear();
		for(int i=0;i<arrayListCapability.size();i++){
		capabilityForm.add(new LabelValueBean(arrayListCapability.get(i).getIdCapability() , arrayListCapability.get(i).getIdCapability()));
		}
		request.setAttribute("capabilityForm", capabilityForm);
		
		
		if(form!=null){
			CreateSDT_Form createSDTForm=(CreateSDT_Form)form;
			capabilitySelezionate=createSDTForm.getSelectedItems();
			
			if(capabilitySelezionate!=null){
				for(int i=0;i<capabilitySelezionate.length;i++)
				System.out.println(capabilitySelezionate[i]);
				AddCapability addCapability=new AddCapability(capabilitySelezionate);
			}
			
			}
		
		
		return mapping.findForward("success");
	}

}