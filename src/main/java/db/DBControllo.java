package db;


import java.sql.ResultSet;
import java.util.ArrayList;

import metadata.DB;

public class DBControllo {
	
	private String idControl;
	private String controlName;
	private String familyId;
	private String familyName;
	private int control;
	private int enhancement;
	private String description;
	
	
	public DBControllo(){
		
	}
	
	public DBControllo(String idControl, String controlName, String familyId, 
			String familyName, int control, int enhancement, String description){
		this.idControl=idControl;
		this.controlName=controlName;
		this.familyId=familyId;
		this.familyName=familyName;
		this.control=control;
		this.enhancement=enhancement;
		this.description=description;
		System.out.println("Stampo id "+idControl);
		System.out.println("Stampo name "+controlName);
		System.out.println("Stampo familyId "+familyId);
		System.out.println("Stampo familyName "+familyName);
		System.out.println("Stampo control "+control);
		System.out.println("Stampo enhancement "+enhancement);
		System.out.println("Stampo la descrizione "+description);
	
		String query="INSERT INTO "+DB.DBName+"."+DB.control+"(idControl,controlName,familyId,familyName,control,enhancement,controlDescription) VALUES('"+idControl+"','"+controlName+"','"+familyId+"','"+familyName+"',"+control+","+enhancement+",'"+description+"');";
		try{
			DBConnectionManager.updateQuery(query);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public ArrayList catturaControlli(String[] familyId){
		ArrayList arrayList = new ArrayList();
		boolean fine=false;
		String query="SELECT * FROM "+DB.DBName+"."+DB.control+" where familyId=";
		for(int i=0;i<familyId.length;i++){
			query=query+"'"+familyId[i]+"'";
			if(i==familyId.length-1){
				fine=true;
			}
			if(!fine){
				query=query+" OR familyId=";
			}
		}
		try {			
			ResultSet rs = DBConnectionManager.selectQuery(query);
			while(rs.next()) {
				arrayList.add(rs.getString("idControl"));
				arrayList.add(rs.getString("controlName"));
				arrayList.add(rs.getString("familyId"));
				arrayList.add(rs.getString("familyName"));
				arrayList.add(rs.getString("control"));
				arrayList.add(rs.getString("enhancement"));
				arrayList.add(rs.getString("controlDescription"));
			}
		} catch(Exception e){
			e.printStackTrace();
		}	
		return arrayList;
			
	}
		
	
	public String getIdControl(){
		return idControl;
	}
	public String getControlName(){
		return controlName;
	}
	public String getFamilyId(){
		return familyId;
	}
	public String getFamilyName(){
		return familyName;
	}
	public int getControl(){
		return control;
	}
	public int getEnhancement(){
		return enhancement;
	}
	private String getDescription(){
		return description;
	}
	

}