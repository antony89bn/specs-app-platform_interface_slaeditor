package db;

import java.sql.ResultSet;
import java.util.ArrayList;

import metadata.DB;

public class DBWsag {
	
	private String idWsag;
	private String wsagDescription;
	
	
	public DBWsag(){
		
	}
	
	public void eliminaWsag(String wsagSelezionati){
		String query="DELETE FROM "+DB.DBName+"."+DB.wsag+" WHERE idWsag='"+wsagSelezionati+"';";
		try{
			DBConnectionManager.updateQuery(query);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
	//scrivo i nuovi Wsag
	public DBWsag(String idWsag, String wsagDescription){
		this.idWsag=idWsag;
		this.wsagDescription=wsagDescription;
		
		String query="INSERT INTO "+DB.DBName+"."+DB.wsag+"(idWsag,wsagDescription) VALUES('"+idWsag+"','"+wsagDescription+"');";
		try{
			DBConnectionManager.updateQuery(query);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
		
	//catturo i nuovi Wsag
	public ArrayList catturaWsag(){
		ArrayList arrayList= new ArrayList();
		String query="SELECT * FROM "+DB.DBName+".wsag ";
		try {			
			ResultSet rs = DBConnectionManager.selectQuery(query);
			while(rs.next()) {
				arrayList.add(rs.getString("idWsag"));
				//System.err.println(rs.getString("wsagDescription"));
				arrayList.add(rs.getString("wsagDescription"));
			}
		} catch(Exception e){
			e.printStackTrace();
		}
		return arrayList;
	}
	
}
